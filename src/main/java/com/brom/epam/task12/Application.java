package com.brom.epam.task12;

import com.brom.epam.task12.view.View;
import com.brom.epam.task12.view.menu.ConsoleMenu;
import java.io.IOException;

public class Application {
  public static void main(String[] args) throws IOException {
    new View().show();
  }
}
