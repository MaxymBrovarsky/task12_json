package com.brom.epam.task12.constant;

public class MenuConstants {
  public static final String SHOW_DATABIND_EXAMPLE = "showgsondatabindexample";
  public static final String SHOW_SORT_EXAMPLE = "showsortexample";
  public static final String QUIT_EXAMPLE = "quit";
}
