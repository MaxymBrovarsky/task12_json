package com.brom.epam.task12.controller;

import com.brom.epam.task12.model.Medicine;
import java.io.IOException;
import java.util.List;

public interface Controller {
  List<Medicine> convertJSONToMedicine(String filePath) throws IOException;
  void showSortExample(String filePath) throws IOException;
}
