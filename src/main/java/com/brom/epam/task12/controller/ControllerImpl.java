package com.brom.epam.task12.controller;

import com.brom.epam.task12.model.Medicine;
import com.brom.epam.task12.service.GsonService;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class ControllerImpl implements Controller {
  private GsonService gsonService = new GsonService();
  @Override
  public List<Medicine> convertJSONToMedicine(String filePath) throws IOException {
    String json = new String(Files.readAllBytes(Paths.get(filePath)));
    return gsonService.convertJSONToMedicine(json);
  }

  @Override
  public void showSortExample(String filePath) throws IOException {
//    gsonService.validate(filePath);
    List<Medicine> medicines = this.convertJSONToMedicine(filePath);
    medicines.sort(Medicine.getNameComparator());
    String json = gsonService.convertMedicineToJSON(medicines);
    Files.write(Paths.get("sorted.json"), json.getBytes());
  }
}
