package com.brom.epam.task12.model;

import java.time.LocalDate;

public class Certificate {
  private long id;
  private LocalDate issueDate;
  private LocalDate endDate;
  private String originOrganization;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public LocalDate getIssueDate() {
    return issueDate;
  }

  public void setIssueDate(LocalDate issueDate) {
    this.issueDate = issueDate;
  }

  public LocalDate getEndDate() {
    return endDate;
  }

  public void setEndDate(LocalDate endDate) {
    this.endDate = endDate;
  }

  public String getOriginOrganization() {
    return originOrganization;
  }

  public void setOriginOrganization(String originOrganization) {
    this.originOrganization = originOrganization;
  }

  @Override
  public String toString() {
    return "Certificate{" +
        "id=" + id +
        ", issueDate=" + issueDate +
        ", endDate=" + endDate +
        ", originOrganization='" + originOrganization + '\'' +
        '}';
  }
}
