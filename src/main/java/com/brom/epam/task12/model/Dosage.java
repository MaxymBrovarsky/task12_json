package com.brom.epam.task12.model;

public class Dosage {
  private int amountForOneUse;
  private String description;

  public int getAmountForOneUse() {
    return amountForOneUse;
  }

  public void setAmountForOneUse(int amountForOneUse) {
    this.amountForOneUse = amountForOneUse;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String toString() {
    return "Dosage{" +
        "amountForOneUse=" + amountForOneUse +
        ", description='" + description + '\'' +
        '}';
  }
}
