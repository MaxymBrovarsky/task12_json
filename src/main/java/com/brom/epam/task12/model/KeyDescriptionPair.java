package com.brom.epam.task12.model;

public class KeyDescriptionPair {
  private int key;
  private String description;

  public int getKey() {
    return key;
  }

  public String getDescription() {
    return description;
  }

}
