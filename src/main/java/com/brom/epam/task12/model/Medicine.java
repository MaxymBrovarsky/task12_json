package com.brom.epam.task12.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Medicine {
  private String name;
  private String pharm;
  private String group;
  private List<String> analogues;
  private String version;
  private Certificate certificate;
  private Dosage dosage;
  private Package pack;

  public Medicine() {
    this.analogues = new ArrayList<>();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPharm() {
    return pharm;
  }

  public void setPharm(String pharm) {
    this.pharm = pharm;
  }

  public String getGroup() {
    return group;
  }

  public void setGroup(String group) {
    this.group = group;
  }

  public List<String> getAnalogues() {
    return analogues;
  }

  public void setAnalogues(List<String> analogues) {
    this.analogues = analogues;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public Certificate getCertificate() {
    return certificate;
  }

  public void setCertificate(Certificate certificate) {
    this.certificate = certificate;
  }

  public Dosage getDosage() {
    return dosage;
  }

  public void setDosage(Dosage dosage) {
    this.dosage = dosage;
  }

  public Package getPack() {
    return pack;
  }

  public void setPack(Package pack) {
    this.pack = pack;
  }

  public void addAnalogue(String analogue) {
    this.analogues.add(analogue);
  }

  @Override
  public String toString() {
    return "Medicine{" +
        "name='" + name + '\'' +
        ", pharm='" + pharm + '\'' +
        ", group='" + group + '\'' +
        ", analogues=" + analogues +
        ", version='" + version + '\'' +
        ", certificate=" + certificate +
        ", dosage=" + dosage +
        ", pack=" + pack +
        '}';
  }

  public static Comparator<Medicine> getNameComparator() {
    return Comparator.comparing(Medicine::getName);
  }
}
