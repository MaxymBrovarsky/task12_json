package com.brom.epam.task12.model;

public class Package {
  private String type;
  private int amountIn;
  private double price;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public int getAmountIn() {
    return amountIn;
  }

  public void setAmountIn(int amountIn) {
    this.amountIn = amountIn;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  @Override
  public String toString() {
    return "Package{" +
        "type='" + type + '\'' +
        ", amountIn=" + amountIn +
        ", price=" + price +
        '}';
  }
}
