package com.brom.epam.task12.service;

import com.brom.epam.task12.constant.ProjectConfig;
import com.brom.epam.task12.model.KeyDescriptionPair;
import com.brom.epam.task12.model.Medicine;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.reflect.TypeToken;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

public class GsonService {
  private Gson gson;

  public GsonService() {
    gson = new GsonBuilder().registerTypeAdapter(LocalDate.class,
        (JsonDeserializer<LocalDate>) (json, type, jsonDeserializationContext) -> LocalDate.parse(json.getAsJsonPrimitive().getAsString())).create();
  }
  public String convertMedicineToJSON(List<Medicine> medicine) {
    return gson.toJson(medicine);
  }

  public List<Medicine> convertJSONToMedicine(String json) {
    Type medicinesListType = new TypeToken<List<Medicine>>() {}.getType();
    return gson.fromJson(json, medicinesListType);
  }

  public Map<String, KeyDescriptionPair> convertJSONToKeyDescriptionPair(String json) {
    Type collectionType = new TypeToken<Map<String, KeyDescriptionPair>>(){}.getType();
    return gson.fromJson(json, collectionType);
  }

  public void validate(String jsonPath) throws FileNotFoundException {
    JSONObject jsonSchema =
        new JSONObject(
            new JSONTokener(
                new FileInputStream(ProjectConfig.schemaPath)));
    JSONObject jsonSubject =
        new JSONObject(
            new JSONTokener(
                new FileInputStream(jsonPath)));
    Schema schema = SchemaLoader.load(jsonSchema);
    try {
      schema.validate(jsonSubject);
    } catch (ValidationException e) {
      e.printStackTrace();
    }
  }
}
