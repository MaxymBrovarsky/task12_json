package com.brom.epam.task12.service;

import com.brom.epam.task12.model.Certificate;
import com.brom.epam.task12.model.Dosage;
import com.brom.epam.task12.model.Medicine;
import com.brom.epam.task12.model.Package;
import com.sun.xml.internal.bind.v2.runtime.reflect.Lister.Pack;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MedicinesHolder {
  private List<Medicine> medicines;
  private Medicine currentMedicine;
  private Map<String,ProcessElementCommand> processingMedicineElementCommands;

  public MedicinesHolder() {
    this.medicines = new ArrayList<>();
    processingMedicineElementCommands = new HashMap<>();
    processingMedicineElementCommands.put("name", this::processMedicineName);
    processingMedicineElementCommands.put("analog", this::processMedicineAnalogs);
    processingMedicineElementCommands.put("certificate", this::processMedicineCertificate);
    processingMedicineElementCommands.put("dosage", this::processMedicineDosage);
    processingMedicineElementCommands.put("group", this::processMedicineGroup);
    processingMedicineElementCommands.put("package", this::processMedicinePackage);
    processingMedicineElementCommands.put("pharm", this::processMedicinePharm);
    processingMedicineElementCommands.put("version", this::processMedicineVersion);
    processingMedicineElementCommands.put("id", this::processCertificateId);
    processingMedicineElementCommands.put("dateofissue", this::processCertificateIssueDate);
    processingMedicineElementCommands.put("enddate", this::processCertificateEndDate);
    processingMedicineElementCommands.put("originorganization", this::processCertificateOriginOrganization);
    processingMedicineElementCommands.put("amountforoneuse", this::processDosageAmountForOneUse);
    processingMedicineElementCommands.put("frequency", this::processDosageFrequency);
    processingMedicineElementCommands.put("type", this::processPackageType);
    processingMedicineElementCommands.put("amountin", this::processPackageAmountIn);
    processingMedicineElementCommands.put("price", this::processPackagePrice);
  }

  private void processMedicineName(String node, Medicine medicine) {
    this.currentMedicine = new Medicine();
    currentMedicine.setName(node);
  }
  private void processMedicinePharm(String node, Medicine medicine) {
    medicine.setPharm(node);
  }
  private void processMedicineGroup(String node, Medicine medicine) {
    medicine.setGroup(node);
  }
  private void processMedicineAnalogs(String node, Medicine medicine) {
    medicine.addAnalogue(node);
  }
  private void processMedicineVersion(String node, Medicine medicine) {
    medicine.setVersion(node);
  }
  private void processMedicineCertificate(String node, Medicine medicine) {
    medicine.setCertificate(new Certificate());
  }

  private void processCertificateId(String node, Medicine medicine) {
    medicine.getCertificate().setId(Long.parseLong(node));
  }

  private void processCertificateIssueDate(String node, Medicine medicine) {
    medicine.getCertificate().setIssueDate(LocalDate.parse(node));
  }
  private void processCertificateEndDate(String node, Medicine medicine) {
    medicine.getCertificate().setEndDate(LocalDate.parse(node));
  }
  private void processCertificateOriginOrganization(String node, Medicine medicine) {
    medicine.getCertificate().setOriginOrganization(node);
  }

  private void processMedicinePackage(String node, Medicine medicine) {
    medicine.setPack(new Package());
  }

  private void processPackageType(String node, Medicine medicine) {
    Package pack = medicine.getPack();
    pack.setType(node);
  }

  private void processPackageAmountIn(String node, Medicine medicine) {
    Package pack = medicine.getPack();
    pack.setAmountIn(Integer.parseInt(node));
  }
  private void processPackagePrice(String node, Medicine medicine) {
    Package pack = medicine.getPack();
    pack.setPrice(Double.parseDouble(node));
  }

  private void processMedicineDosage(String node, Medicine medicine) {
    medicine.setDosage(new Dosage());
  }

  private void processDosageAmountForOneUse(String node, Medicine medicine) {
    Dosage dosage = medicine.getDosage();
    dosage.setAmountForOneUse(Integer.parseInt(node));
  }

  private void processDosageFrequency(String node, Medicine medicine) {
    Dosage dosage = medicine.getDosage();
    dosage.setDescription(node);
    this.medicines.add(currentMedicine);
  }
  public void processElement(String tagName, String value) {
    if (processingMedicineElementCommands.containsKey(tagName)) {
      this.processingMedicineElementCommands.get(tagName).execute(value, this.currentMedicine);
    }
  }

  public List<Medicine> getMedicines() {
    return medicines;
  }
}
