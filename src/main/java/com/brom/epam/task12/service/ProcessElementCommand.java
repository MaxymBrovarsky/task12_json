package com.brom.epam.task12.service;

import com.brom.epam.task12.model.Medicine;

public interface ProcessElementCommand {
  void execute(String node, Medicine medicine);
}
