package com.brom.epam.task12.view;

import com.brom.epam.task12.view.menu.ConsoleMenu;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {
  private ConsoleMenu consoleMenu;
  private Scanner input = new Scanner(System.in);
  private Logger logger = LogManager.getLogger(View.class.getName());

  public View() throws IOException {
    this.consoleMenu = new ConsoleMenu();
  }

  public void show() {
    while(true) {
      logger.info(consoleMenu.show());
      try {
        int commandKey = input.nextInt();
        input.nextLine();
        consoleMenu.executeCommand(commandKey);
      } catch (InputMismatchException e) {
        logger.error(e.getMessage());
      }
    }
  }
}
