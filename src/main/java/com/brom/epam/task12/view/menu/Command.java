package com.brom.epam.task12.view.menu;

import com.brom.epam.task12.controller.Controller;
import com.brom.epam.task12.controller.ControllerImpl;
import java.util.Scanner;

public abstract class Command {
  protected int key;
  protected String description;
  protected Controller controller;
  protected Scanner input;

  public Command(int key, String description) {
    this.key = key;
    this.description = description;
    this.controller = new ControllerImpl();
    this.input = new Scanner(System.in);
  }
  public abstract void execute();

  public int getKey() {
    return key;
  }

  @Override
  public String toString() {
    return key + ". " + description;
  }
}
