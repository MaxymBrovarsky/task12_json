package com.brom.epam.task12.view.menu;

import com.brom.epam.task12.constant.MenuConstants;
import com.brom.epam.task12.constant.ProjectConfig;
import com.brom.epam.task12.model.KeyDescriptionPair;
import com.brom.epam.task12.service.GsonService;
import com.brom.epam.task12.view.menu.commands.QuitCommand;
import com.brom.epam.task12.view.menu.commands.ShowGSONDatabindExampleCommand;
import com.brom.epam.task12.view.menu.commands.ShowSortExampleCommand;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ConsoleMenu {
  private List<Command> commands;

  public ConsoleMenu() throws IOException {
    GsonService gsonService = new GsonService();
    String json = new String(Files.readAllBytes(Paths.get(ProjectConfig.menuPath)));
    Map<String, KeyDescriptionPair> commandInfo =
        gsonService.convertJSONToKeyDescriptionPair(json);
    initMenu(commandInfo);
  }

  public void initMenu(Map<String, KeyDescriptionPair> commandsInfo) {
    commands = new ArrayList<>();
    KeyDescriptionPair showDatabindInfo = commandsInfo.get(MenuConstants.SHOW_DATABIND_EXAMPLE);
    commands.add(new ShowGSONDatabindExampleCommand(
            showDatabindInfo.getKey(), showDatabindInfo.getDescription()
            ));
    KeyDescriptionPair showSortInfo = commandsInfo.get(MenuConstants.SHOW_SORT_EXAMPLE);
    commands.add(new ShowSortExampleCommand(
        showSortInfo.getKey(), showSortInfo.getDescription()
    ));
    KeyDescriptionPair quitInfo = commandsInfo.get(MenuConstants.QUIT_EXAMPLE);
    commands.add(new QuitCommand(
            quitInfo.getKey(), quitInfo.getDescription()
            ));

  }

  public void executeCommand(int key) {
    commands.stream()
        .filter(c -> c.key == key)
        .findFirst()
        .ifPresent(c -> c.execute());
  }

  public String show() {
    String result =  commands.stream().map(c -> c.toString()).collect(Collectors.joining("\n"));
    return result;
  }
}
