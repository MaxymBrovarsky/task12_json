package com.brom.epam.task12.view.menu.commands;


import com.brom.epam.task12.view.menu.Command;

public class QuitCommand extends Command {

  public QuitCommand(int key, String description) {
    super(key, description);
  }
  @Override
  public void execute() {
    System.exit(0);
  }
}
