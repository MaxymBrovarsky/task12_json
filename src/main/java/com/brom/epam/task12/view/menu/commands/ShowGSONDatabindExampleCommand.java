package com.brom.epam.task12.view.menu.commands;

import com.brom.epam.task12.model.Medicine;
import com.brom.epam.task12.view.menu.Command;
import java.io.IOException;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ShowGSONDatabindExampleCommand extends Command {
  private Logger logger = LogManager.getLogger(ShowGSONDatabindExampleCommand.class.getName());
  public ShowGSONDatabindExampleCommand(int key, String description) {
    super(key, description);
  }

  @Override
  public void execute() {
    logger.info("Please enter file path");
    String filePath = input.nextLine();
    try {
      List<Medicine> medicines = controller.convertJSONToMedicine(filePath);
      logger.info(medicines);
    } catch (IOException e) {

    }
  }
}
