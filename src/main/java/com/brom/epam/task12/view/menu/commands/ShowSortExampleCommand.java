package com.brom.epam.task12.view.menu.commands;

import com.brom.epam.task12.view.menu.Command;
import java.io.IOException;

public class ShowSortExampleCommand extends Command {

  public ShowSortExampleCommand(int key, String description) {
    super(key, description);
  }
  @Override
  public void execute() {
    String filePath = input.nextLine();
    try {
      controller.showSortExample(filePath);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
